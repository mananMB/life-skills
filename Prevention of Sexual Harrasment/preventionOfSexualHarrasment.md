# Prevention of Sexual Harassment


### What kinds of behaviour cause sexual harassment?

- Making sexual jokes.
- Quid Pro Quo.
- Repeated remarks about an employee's appearance.
- Commenting on the appearance of others in front of an employee.
- Discussing someone's sex life in front of a colleague.
- Circulating photos of pornographic nature.
- Interrogating an employee about their sex life.
- Leaving unwanted gifts of a sexual or romantic nature.
- Spreading sexual rumours about a colleague.
- Inappropriate and unwanted touching.

### What would you do in case you face or witness any incident or repeated incidents of such behaviour?

- Act as witness for the victim.
- Intervene and call out the harasser if it is safe to do so.
- Create a distracting situation to stop the harassment from continuing.
- Report it to the HR Department.
 