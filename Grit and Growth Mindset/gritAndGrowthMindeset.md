# Grit and Growth Mindset

## 1. Grit

#### Question 1

Learning things and getting good at them is not about IQ, it is about how much effort one puts learning or getting better at things one is working on. To get better at learning or growing and better in life one requires **Grit**. Grit is about having passion and perseverance for things we are trying to achieve. Ability to learn is not fixed and it changes depending on effort that is put in.

#### What are your key takeaways from the video to take action on?

- Keep yourself motivated
- Rome wasn't built in a day
- Don't give up on goals in short term
- Follow through on your commitment

## 2. Introduction to Growth Mindset

#### Question 3

Growth Mindset is about belief in one's ability to grow. People are in control of their skills and can grow and get better at those skills with the effort they put in practicing said skills. Growth mindset is not boolean, it is a spectrum 

#### What are your key takeaways from the video to take action on?

- Growth Mindset is the foundation for learning
- Focus should be on process of getting better
- Effort should be put in to get better
- Don't back down or avoid
- Accept feedbacks
- Appreciate feedbacks

## 3. Understanding Internal Locus of Control

#### What is the Internal Locus of Control? 

The degree to which we believe we have control over our life.

#### What is the key point in the video?

The key to staying motivated is about believing that we have control over our life. If we can see that factors we change achieves a better result, it keeps us motivated. If we believe that no matter what we do the result will not change, it drives our motivation down.

## 3. How to build a Growth Mindset

#### Question 6

A lot of people have a fixed mindset, and they believe they can't get better at anything with any amount of effort they put in it. 

#### What are your key takeaways from the video to take action on?

- Always believe in your ability to figure things out
- Question your assumptions

## 4. Mindset - A MountBlue Warrior Reference Manual

#### Question 8 What are one or more points that you want to take action on from the manual?

- I know more efforts lead to better understanding
- I will wear confidence in my body. I will stand tall and sit straight
