# Listening and Active Communication

## 1. Active Listening

#### What are the steps/strategies to do Active Listening?

- Pay attention
  - Look at the speaker directly
  - Don't get distracted with anything else
- Show that you are listening
  - Nod 
  - Facial Expressions
  - Body language
    - Posture
    - Smile
- User door-openers
  - 'Tell me more'
  - 'That sounds interesting'
- Don't interrupt
- Provide feedback
  - "What I'm hearing is... ," and "Sounds like you are saying... ," are great ways to reflect back.

## 2. Reflective Listening

#### According to Fisher's model, what are the key points of Reflective Listening?

Focusing on the conversation is essential part of Reflective Listening. 
- Distractions should be eliminated
- Perspective of the speaker should be considered without any judgement
- Responding to each subject
- Switching roles of speaker and listener if needed
- Reflecting mood of the speaker in context of the subject

## 3. Reflection

#### What are the obstacles in your listening process?

- ADHD

#### What can you do to improve your listening?

- Try to control my thoughts
- Respond in a manner without going out of subject context
- Provide feedback

## 4. Types of Communication
* Passive Communication
* Aggressive Communication
* Passive Aggressive Communication
* Aggressive Communication
* 
#### When do you switch to Passive communication style in your day to day life?

- Occasionally

#### Question 6
When do you switch into Aggressive communication styles in your day to day life?

- Rarely

#### Question 7
When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

- Never

#### Question 8
How can you make your communication assertive?

- Use 'I' statements
- Practice saying no
- Use body language
- Keep emotions in check i.e. don't become aggressive or burst out.