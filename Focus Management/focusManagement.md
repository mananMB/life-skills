# Focus Management

## 1. What is Deep Work

#### Question 1: What is Deep Work?

- Uninterrupted focus on work with **zero** distractions
- This helps to build skills and produce work of impeccable quality

## 4. Summary of Deep Work Book

#### Question 2: Paraphrase all the ideas in the above videos and this one **in detail**.

- Intense periods of focus produces myelin in relevant areas of brain, and it allows neurons to fire faster
- Ability to Deep Work is valuable and rare. Distractions happen every few minutes and break the flow of work which messes with our focus while working
- Distractions can be minimized by scheduling distractions 
- Set boundaries
- Deep Work can be learnt. We can start it by scheduling it in phases and small durations around 60 to 90 minutes
- Early morning is best time to start deep working is in the morning when distractions are minimum
- Have shut down rituals. Create action plan for next day

#### Question 3: How can you implement the principles in your day-to-day life?

- Start with small duration of 60 minutes
- Keep all distraction at bay during those 60 minutes
- Do it everyday
- Gradually increase the duration of deep work sessions
- 
## 5. Dangers of Social Media

#### Question 4: Your key takeaways from the video

- Distractions
- Unrealistic Expectations looking at others sharing life, truth or lie
- They are an entertainment product
- We are just products for ad agencies
- Designed to be addictive
- FOMO