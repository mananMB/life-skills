# SOLID Principles

## What are SOLID Principles?

SOLID is a set of design principles in object-oriented software development.
It is an acronym for the following five design principles:

- The **S**ingle Responsibility Principle
- The **O**pen/Closed Principle
- The **L**iskov Substitution Principle
- The **I**nterface Segregation Principle
- The **D**ependency Inversion

## Why use SOLID Principles?

The goal of the SOLID principles is to reduce dependencies so that change to one aspect of software will not affect others. Additionally, they are intended to make designs patterns which are easier to understand, maintain, and extend.

The use of these design principles makes it easier for software engineers to mitigate issues and to build effective, and agile software.

## What are the SOLID Principles?

1. #### The Single Responsibility Principle
   - A class, function, interface, abstraction should only have responsibility to do one task and therefore have only a single reason to change.
   
     - If a Class has many responsibilities, chances are changing one responsibility will affect others without you knowing.

   This aims to separate behaviours so that if bugs arise because of your change, it will not affect other unrelated behaviours.
   
2. #### The Open/Closed Principle

   - Classes should be open for extension and closed to modification.

      - Changing the current behaviour of a Class will affect all the other Classes that depend on it.

      - If you want the Class to perform more functions, you should extend it by adding more features instead of modifying the existing ones.

   This aims to extend a Class’s behaviour without changing the existing behaviour of that Class. This is to avoid causing bugs wherever the Class is being used.

3. #### The Liskov Substitution Principle

    - Child classes should be substitutable for their Parent classes.

      - When a **child** Class cannot perform the same actions as its **parent** Class, this might cause issues.

      - The **child** Class should be able to perform the same tasks and provide the same result as it's **parent** Class, or it could deliver a result that is of the same type.

   This aims to enforce consistency so that the parent Class or its child Class can be used in the same way without any errors.

4. #### The Interface Segregation Principle
   
   - Interface Segregation Principle is about separating the interfaces.

     - When a Class must perform actions which are not useful for actions they are designed to perform, it is a waste of resources and may produce unexpected bugs if the Class does not have the ability to perform those actions.

     - A Class should only perform actions that are needed to fulfil its role and not perform any other unnecessary actions that are not needed to fulfil its role. This is to avoid wasting resources and to avoid bugs.

   This aims to split set of actions into smaller sets, so the Class only executes small sets of actions. This also works towards fulfilling Single Responsibility Principle.
   
5. #### The Dependency Inversion Principle
   
   - Classes should depend upon interfaces or abstract classes instead of concrete classes and functions.

     - High-level modules should not depend on low-level modules. Both should depend on the abstraction.

     - Abstractions should not depend on details. Details should depend on abstractions. If a Class is meant to cut vegetables with a tool, it should do it with any tool that can cut vegetables, not just a knife.

   This aims at reducing the dependency of a parent Class on the child Class by introducing an **interface**.

## Explanations with examples

#### 1. Single Responsibility Principle. 

---

Each class should have only one purpose.

```
public class PasswordHasher
{
    public String hashAndSavePassword(String password)
    {
        hashPassword();
        savePassword();
    }

    public void hashPassword()
    {
        //hashing implementation
    }
    public void savePassword()
    {
        //save to the db
    }
}
```

Let us implement a class for hashing passwords. It should be its only responsibility and nothing else like also saving them to the database.

In this example, the class `PasswordHasher` has two responsibilities: hashing and saving a password. This violates the Single Responsibility Principle.

The class should be split into two classes: `PasswordHasher` and `PasswordSaver`.

```
public class PasswordHasher
{
    public String hashPassword(String password)
    {
        //hashing implementation
    }
}
```

```
public class PasswordSaver
{
    public void savePassword(String password)
    {
        //save to the db
    }
}
```

There should not be any class that have a various variety of functionality and has too much to do. Instead, we should write our classes as modular as possible.

#### 2. Open-closed Principle

--- 

Classes should be open for extension, closed for modification.

Continuing with the password-hasher example, let us say we want to add a new feature to the class. We want to add a new hashing algorithm to the class.

```
public String hashPassword(String password, HashingType hashingType)
{
    if(HashingType.BASE64.equals(hashingType))
    {
        hashedPassword="hashed with Base64";
    }
    else if(HashingType.MD5.equals(hashingType))
    {
        hashedPassword="hashed with MD5";
    }

    return hashedPassword;
}
```

In this example, we would have to add a new `if` statement to the `hashPassword` method every time we want to add a new hashing algorithm. This violates the Open-Closed Principle.

We should make our first class an interface/abstract class and implement the algorithms in the concrete classes.

```
public interface PasswordHasher
{
    String hashPassword(String password);
}
```

```
public class Base64Hasher implements PasswordHasher
{
    @Override
    public String hashPassword(String password)
    {
        return "hashed with 64";
    }
}
```

```
public class MD5Hasher implements PasswordHasher
{
    @Override
    public String hashPassword(String password)
    {
        return "hashed with SHA256";
    }
}
```

In this way, we can add new algorithms without touching the existing codebase.

#### 3. Liskov Substitution Principle

---

A subclass should be able to fulfil each feature of its parent class and could be treated as its parent class.

Let us create the Model (Data) Classes to use our hashing algorithms called `Hashed`

```
public abstract class Hashed
{
    PasswordHasher passwordHasher;
    String hash;
    
    public void generateHash(String password)
    {
        hash = passwordHasher.hashPassword(password);
    }
}
```

```
public class Base64 extends Hashed
{
    public Base64()
    {
        this.passwordHasher = new Base64Hasher();
    }
}
```
And we implement the same for other encodings.

To fulfil Liskov’s Rule, each other extension of `Hashed` should use a valid implementation of hashing function and return a hash.

For example, if we extend the Hashed class with a class called `NoHash` that uses an implementation that returns the same password without any encoding will break the rule, since a subclass of `Hashed` is expected to have a hashed value of the password.

#### 4. Interface Segregation Principle

---

Interfaces should not force classes to implement what they cannot do. Large interfaces should be divided into small ones.

Let us say we want to add a new decoding feature to our `PasswordHasher` interface. We want to add a new method called `decodePasswordFromHash()` that returns the decoded password.

```
public interface PasswordHasher
{
    String hashPassword(String password);
    String decodePasswordFromHash(String hash);
}
```

This will force all the classes that implement the `PasswordHasher` interface to implement the `decodePasswordFromHash()` method. Some of the hashing functions are one-way and cannot be decoded. This violates the Interface Segregation Principle.

Instead, we can add another interface to the applicable classes to implement their decoding algorithm.

```
public interface Decryptable
{
    String decodePasswordFromHash(String hash);
}
```

```
public class Base64Hasher implements PasswordHasher, Decryptable
{
    @Override
    public String hashPassword(String password)
    {
        return "hashed with base64";
    }

    @Override
    public String decodePasswordFromHash(String hash)
    {
        return "decoded from base64";
    }
}
```

#### 5. Dependency Inversion Principle

---

Components should depend on abstractions, not on concretions.

We have a password service like the following:

```
public class PasswordService
{
    private Base64Hasher hasher = new Base64Hasher();
    void hashPassword(String password)
    {
        hasher.hashPassword(password);
    }
}
```

We violated this principle since now the `Base64Hasher` is tightly coupled with the `PasswordService`. If we want to change the hashing algorithm, we must change the `PasswordService` class.

Let us decouple them and let the client inject the hashing service needed with the constructor.

```
public class PasswordService
{
    private PasswordHasher passwordHasher;
    
    public PasswordService(PasswordHasher passwordHasher)
    {
        this.passwordHasher = passwordHasher;
    }
    
    void hashPassword(String password)
    {
        this.passwordHasher.hashPassword(password);
    }
}
```

We can easily change the hashing algorithm. Our service does not care about the algorithm, it is up to the client to choose it. We do not depend on the concrete implementation, but the abstraction.

## Conclusions

Projects that adhere to SOLID principles can be shared with collaborators, extended, modified, tested, and refactored with fewer complications.

## References

- [A Solid Guide to SOLID Principles - Baeldung](https://www.baeldung.com/solid-principles)

- [SOLID: The First 5 Principles of Object Oriented Design - Digital Ocean](https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design)

- [The SOLID Principles of Object-Oriented Programming Explained in Plain English - freeCodeCamp](https://www.freecodecamp.org/news/solid-principles-explained-in-plain-english/)

- [Markdown Basic Syntax](https://www.markdownguide.org/basic-syntax/)