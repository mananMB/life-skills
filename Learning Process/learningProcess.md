# Learning Process

## 1. How to Learn Faster with the Feynman Technique

#### Question 1

What is the Feynman Technique?

It is a learning process designed to make us learn things in an efficient way.

#### Question 2

What are the different ways to implement this technique in your learning process?

1. Pick a topic and study
2. Explain it to someone unfamiliar with the topic in a simple language
3. Identify issues in your understanding
4. Study the topic again to clarify your issues in your understanding
5. Repeat from step 2


## 2. Learning How to Learn TED talk by Barbara Oakley

#### Question 3

There are two modes in our brain.
- Focus Mode
- Diffuse Mode

Pomodoro Technique
- Set a timer for 25 minutes
- Turn off all the distractions
- Work in Focused Mode for 25 minutes
- After 25 minutes of work, do something fun for few minutes
- This helps with practicing both focused mode of working and relaxation

Understanding alone isn't enough to build mastery of the material.

#### Question 4

What are some of the steps that you can take to improve your learning process?

- Don't get distracted
- Practice daily
- Test yourself all the time
- Practice topics until the solution plays from your mind like a song
- Look at a page, look away, see what you can recall

## 3. Learn Anything in 20 hours

#### Question 5

It does not take 10,000 hours to learn something, it takes little time to get reasonable good at something with consistent practice daily.

- Barrier to learning new thing is not Intellectual, it is emotional.

#### Question 6

What are some of the steps that you can while approaching a new topic?

- Deconstruct the skill
- Learn enough to self-correct
- Remove distractions during practice
- Practice consistently, everyday.
- Practice at least 45 minutes a day.